/*const mongoose = require("mongoose"); //se requiere el paquete para que funcione

mongoose.connect("mongodb://localhost:27017/MiDulceOnline", { //nombre de la base de ddatos en robot
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
    }, (err, res) => { //promesa
        if (err) {
            throw
            err
        } else {
            console
            log('La conexion a la base de datos fue correcta...')
        }
    });

module.exports = mongoose; //se exporta para poder usar el archivo en el proyecto
*/
const mongoose = require("mongoose"); //Llamado a la dependencia mongoose
mongoose.connect( //Comando para conectarse con la base de datos en el caso 
  "mongodb://localhost:27017/MiDulceOnline",
  (err, res) => {
    if (err) {
      throw err;
    } else {
      console.log("La conexion a la base de datos fue correcta...");
    }
  }
);
module.exports = mongoose;