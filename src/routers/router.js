const {Router} = require('express');

var controllerProducto=require('../controllers/controllerProducto')
var controllerVenta=require('../controllers/controllerVenta')
var controllerCliente=require('../controllers/controllerCliente')
const postController = require('../controllers/postController');

const router=Router();

//PRODUCTO
router.get('/producto/test',controllerProducto.producto);
router.get('/producto/listar', controllerProducto.listar);
router.post('/crear',controllerProducto.guardarProducto);
router.get('/post/list', postController.list );
router.post('/post/save', postController.savePost);

//VENTA
router.get('/venta/test',controllerVenta.venta);
//CLIENTE
router.get('/cliente/test',controllerCliente.cliente);
/*router.post('/ciente/create', controllerCliente.saveUser);//Ruta con método post para crear cuenta de usuario
router.post('/cliente/login',controllerCliente.login); //Ruta con método post parra loggearse y obtener token

//router.put('/cliente/update/:id', controllerTokens.verifyToken, controllerUsers.updateUser);//Ruta con método put para actualizar un usuario (no terminado)

router.get('/cliente/search/:id',controllerCliente.buscarData);//Ruta con método get para traer un expert por id
router.get('/cliente/all/:id?', controllerCliente.listarAllData);//Ruta con método get para traer todos los experts o por id también
router.delete('/cliente/delete/:id', controllerCliente.deleteUser);//Ruta con método delete para eliminar un expert
router.put('/cliente/update/:id',controllerCliente.updateUser);//Ruta con método put para actualizar un expert
*/

module.exports=router;

/*var controllerProducto=require('../controllers/controllerProducto');
var controllerCliente = require('../controllers/controllerCliente');


const authController = require('../controllers/authController');
const userController = require('../controllers/userController');

let router = Router();

// Posts RUTAS
router.get('/post/')
router.get('/post/list/:search?', postController.listPosts); //RECIBE UNA SOLICITUD GET A UNA RUTA EN POST DONDE DESPUES DE ESTA RUTA VENGA UN PARAMETRO ID 
router.get('/post/:id', postController.findPost); // toca ir a post controller y crear el metodo 
router.post('/post/save', postController.savePost);
router.put('/post/:id', postController.updatePost);
router.delete('/post/:id', postController.deletePost);

// Auth
router.post('/auth/login', authController.login);
router.post('/auth/test', authController.verifyToken, authController.test);

// Users
router.post('/user/save', userController.saveUser);

module.exports = router;
*/