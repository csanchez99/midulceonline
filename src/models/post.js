const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
    title: { type: String, required: true },
    content: { type: String, required: true },
    user: { type: String, required: true },
    created: { type: Date, default: Date.now }
});

const Post = mongoose.model('posts', postSchema);

/*const productSchema = new Schema({
    name: String,
    origin: String,
    cost: String,
    ingredients: { type: [String], index: true } // path level
});
  productSchema.index({ name: 1, type: -1 }); // schema level
*/

module.exports = Post;