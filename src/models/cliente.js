var mongoose = require('mongoose');
var Schema = mongoose.Schema; //permite definir los campos almacenados en cada documento junto con sus requisitos de validación y valores predeterminados

var clienteSchema = Schema({

    nombre:String,
    cedula:String,
    direccion:String,
    telefono:String,
    correo:String,
});

/*    nombre:String,
    escuela:String,
    universidad:String
    });
    */
//. Definimos que lo vamos a usar en nuestra aplicación como un modulo
const Cliente = mongoose.model('Cliente', clienteSchema);
module.exports = Cliente;