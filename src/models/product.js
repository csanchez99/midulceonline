var mongoose = require('mongoose');
var Schema = mongoose.Schema; //permite definir los campos almacenados en cada documento junto con sus requisitos de validación y valores predeterminados

var productoSchema = Schema({

    artesanal: Boolean,
    nombre: String,
    foto: String,
    precio: Number,
    cantidadDisponible: Number,
    nombreFabricante: String,
    ingredientes: String,
    lugarOrigen: String,
    Artesanal: Boolean
});

/*    nombre:String,
    escuela:String,
    universidad:String
    });
    */
//. Definimos que lo vamos a usar en nuestra aplicación como un modulo
const Producto = mongoose.model('Producto', productoSchema);
module.exports = Producto;