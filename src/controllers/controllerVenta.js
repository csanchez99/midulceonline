var Venta = require('../models/venta')

function venta(req, res) {
    res.status(200).send({
        message: 'probando una venta'
    })
}

function listar(req, res) {
    var consulta = Venta.find({});
    consulta.exec(function (err, result) {
        if (err) {
            res.send(err)
        } else {
            if (!result) {
                res.send({ message: "No hay registros" })
            } else {
                res.send({ result })
            }
        }
    })
}

module.exports = {
    venta,
    listar
}