//var Cliente = require('../models/cliente')
var Cliente = require('../models/cliente')
var bcrypt = require("bcrypt-nodejs");
const jwt = require("jsonwebtoken");

//probar el controlador
function cliente(req, res) {
    res.status(200).send({
        message: 'probando un cliente'
    })
}

//crear un usuario
function guardarCliente(req, res) {
    var user = req.body;
    user.createDate = new Date();
    user.isExpert = false;
    user.password = bcrypt.hashSync(user.password);
    var myUser = new User(user);
    myUser.save((err, result) => {
        if (err) {
            res.send({
                error: err,
                isAccept: false
            });
        } else {
            res.status(200).send({
                message: result,
                isAccept: true
            });
        }
    });
}

function guardarCliente(req, res) {
    var cliente = Venta.find({});
    consulta.exec(function (err, result) {
        if (err) {
            res.send(err)
        } else {
            if (!result) {
                res.send({
                    message: "No hay registros"
                })
            } else {
                res.send({
                    result
                })
            }
        }
    })
}



//Login de usuario
function login(req, res) {
    const user = req.body;
    var result = User.find({
        nickname: user.nickname
    });
    result.exec(function (err, result) {
        if (err) {
            res
                .status(500)
                .json({
                    message: "Error al momento de ejecutar la solicitud"
                });
        } else {
            if (!result) {
                res
                    .send({
                        message: "El usuario y/o contraseña son incorrectos",
                        access: false
                    });
            } else {
                if (result[0] != null) {
                    if (bcrypt.compareSync(user.password, result[0].password)) {
                        jwt.sign({
                                user: user
                            },
                            "secretKey", {
                                expiresIn: "1h"
                            },
                            (err, token) => {
                                res.status(200).json({
                                    token: token,
                                    access: true
                                });
                            }
                        );
                    } else {
                        res
                            .json({
                                message: "El usuario y/o contraseña son incorrectos",
                                access: false
                            });
                    }
                } else {
                    res
                        .json({
                            message: "El usuario y/o contraseña son incorrectos",
                            access: false
                        });
                }
            }
        }
    });
}

function updateUser(req, res) {
    jwt.verify(req.token, "secretKey", (error, authData) => {
        if (error) {
            res.status(403);
        } else {
            var id = req.params.id;
            User.findOneAndUpdate({
                _id: id
            }, req.body, {
                new: true
            }, function (err, expert) {
                if (err) {
                    res.send(err);
                } else {
                    res.json({
                        message: "Usuario actualizado"
                    });
                }
            });
        }
    });
}

function buscarData(req, res) {
    var idExpert = req.params.id;
    console.log(idExpert);
    User.findById(idExpert).exec((err, result) => {
        console.log(result);
        if (err) {
            res
                .status(500)
                .send({
                    message: "Error al momento de ejecutar la solicitud"
                });
        } else {
            if (!result) {
                res
                    .status(404)
                    .send({
                        message: "El registro a buscar no se encuentra disponible"
                    });
            } else {
                res.status(200).send({
                    result
                });
            }
        }
    });
}

function listarAllData(req, res) {
    var idExpert = req.params.id;
    if (!idExpert) {
        var result = User.find({}).sort("firstname");
    } else {
        var result = User.find({
            _id: idExpert
        }).sort("firstname");
    }
    result.exec(function (err, result) {
        if (err) {
            res
                .status(500)
                .send({
                    message: "Error al momento de ejecutar la solicitud"
                });
        } else {
            if (!result) {
                res
                    .status(404)
                    .send({
                        message: "El registro a buscar no se encuentra disponible"
                    });
            } else {
                res.status(200).send({
                    result
                });
            }
        }
    });
}

function deleteUser(req, res) {
    var id = req.params.id;
    User.findByIdAndRemove(id, function (err, expert) {
        if (err) {
            return res.json(500, {
                message: "No hemos encontrado el experto",
            });
        }
        return res.json(expert);
    });
}
module.exports = {
    Cliente,
    cliente,
    guardarCliente,
    login,
    buscarData,
    listarAllData,
    updateUser,
    deleteUser,
}

/*module.exports = {
    Cliente,
    pruebaCliente,
   }
   */