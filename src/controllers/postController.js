let Post = require('../models/post');

function list(req, res) {
        let query = Post.find({}).sort('content');
        query.exec((err, result) =>{
            if(err){
                res.status(500).send({message: err});
            }
        });
}

function savePost(req, res){
    let myPost = new Post( req.body );
    myPost.save( (err, result ) => {
        if(err){
            res.status(500).send( { message: err} );
        }else{
            res.status(200).send( { message: result} );
        }
    });
}

module.exports = { list, savePost }
