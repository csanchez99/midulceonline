var Producto = require('../models/product')

function producto(req, res) {
    res.status(200).send({
        message: 'probando un producto'
    })
}

function listar(req, res) {
    var consulta = Producto.find({});
    consulta.exec(function (err, result) {
        if (err) {
            res.send(err)
        } else {
            if (!result) {
                res.send({
                    message: "No hay registros"
                })
            } else {
                res.send({
                    result
                })
            }
        }
    })
}

function guardarProducto(req, res) {
    var nuevoProducto = new producto(req.body);
    nuevoProducto.save((err, result) => {
        res.status(200).send({message: result});
    });
}

module.exports = {
    producto,
    listar,
    guardarProducto
}

